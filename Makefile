BINARY_NAME=server

build:
	go build -ldflags "-X main.appVersion=v1.0.0 -X main.commit=$(shell git rev-parse HEAD) -X 'main.buildTime=$(shell date --rfc-3339=seconds)'" -o ${BINARY_NAME}


